---
title: TALP
subtitle: Chapter 5
date: 2020-04-24
tags: ["talp", "programming", "lambda", "calculus"]
---

Types and Programming Languages
===============================

5.2 Programming in Lambda-Calculus
----------------------------------

### Evaluation strategy ###
TALP uses "call-by-value" (p. 79), however I could not getting it the first time I read it, nor the second. There is a quite simple explanation at [CSE 40431 - Untyped \\(\lambda\\)](https://www3.nd.edu/~dchiang/teaching/pl/2019/lambda.html). I will just cite it:
> Call-by-value is just what you're used to from most (if not all) programming languages you've used. It has three rules.
> - No reductions inside abstractions.
> - A redex is reduced only when its right-hand side has already been reduced to a value, that is, a variable or abstraction.
> - Subject to the above rules, the leftmost redex is reduced first. (The book doesn't make this rule entirely clear.)
> 
> It may be easier to think about this way:
> - To evaluate an abstraction \\(\lambda x. t\\), do nothing.
> - To evaluate an application \\(t_1\ t_2\\), first evaluate \\(t_1\\), then evaluate \\(t_2\\), then \\(\beta\\)-reduce the application.

### Church Booleans ###

\\[
\text{tru} = \lambda t. \lambda f. t
\\]
\\[
\text{fls} = \lambda t. \lambda f. f
\\]

Also is useful to define \\(\text{test} = \lambda b. \lambda v. \lambda w. b\ v\ w\\). Then \\(\text{test}\ \text{tru}\ v\ w = v\\) and \\(\text{test}\ \text{fls}\ v\ w = w\\).

Right after reading that boolean operators can be defined using the previous definitions, I wanted to try to come with my own definitions of \\(\text{and}\\), \\(\text{or}\\), etc. After stealing the idea of "use the parameters to decide" from \\(\text{test}\\), I came with this:
\\[
\text{and}\ x\ y = x \left( y\ \text{tru}\ \text{fls} \right) \text{fls}
\\]
Which can be interpreted as:
```
if x == True
  if y == True
    return True
  else
    return False
else
  return False
```
Clearly, the inner `if` statement can be replaced by `return y`. Then \\(\text{and}\ x\ y = x\ y\ \text{fls}\\), just like the book definition. Then \\(\text{or}\ x\ y = x\ \text{tru}\ y\\), \\(\text{not}\ x = x\ \text{fls}\ \text{tru}\\). A, probably useful, helper function could be \\(\text{areEqual}\ x\ y = x \left( y\ \text{tru}\ \text{fls} \right) \left( y\ \text{fls}\ \text{tru} \right) = x\ y\ \left( y\ \text{fls}\ \text{tru} \right)\\).

#### Exercise 5.2.1 ####
\\[
\text{or}\ x\ y = x\ \text{tru}\ y
\\]
\\[
\text{not}\ x = x\ \text{fls}\ \text{tru}\ 
\\]

### Pairs ###
\\[
\text{pair} = \lambda f. \lambda s. \lambda b. b\ f\ s
\\]
\\[
\text{fst}\  = \lambda p. p\ \text{tru}
\\]
\\[
\text{snd}\  = \lambda p. p\ \text{fls}
\\]

A \\(\text{pair}\ a\ b\\) just stores \\(a\\) and \\(b\\). With \\(\text{fst} \left( \text{pair}\ a\ b \right) = a\\) and \\( \text{snd} \left( \text{pair}\ a\ b \right) = b\\) we can retrive then.

### Church Numerals ###

\\[
C_0 = \lambda s. \lambda z. z
\\]
\\[
C_1 = \lambda s. \lambda z. s\ z
\\]
\\[
C_2 = \lambda s. \lambda z. s \left( s\ z \right)
\\]
\\[
C_i = \lambda s. \lambda z. s \left( C_{i-1}\ s\ z \right)
\\]

Their trivial interpretation is "apply function \\(s\\) \\(i\\)-times starting from \\(z\\)".

Lets find the __succesor function__, \\(\text{succ}\ C_i = C_{i+1}\\). It should take a Church Numeral, a binary function, and return another Church Numeral. So \\( \text{succ}\ C_i = \left( \lambda x. \lambda s. \lambda z. \? \right) C_i = \lambda s. \lambda z. s^{i+1} z = C_{i+1}\\). Then the idea is just apply the \\(s\\) parameter over the result of \\(C_i\ s\ z\\). Finally, \\(\text{succ}\ C_i = \lambda x. \lambda s. \lambda z. s \left( x s i \right)\\).

#### Exercise 5.2.2 ####

> Another way to encode "apply \\(s\\) one more time" is to apply it to \\(z\\) _before_. So \\(\text{succ}\ C_i = \lambda x. \lambda s. \lambda z. x\ s\ \left( s z \right)\\). Another idea I came with is to "reuse the \\(\lambda z\\) of the \\(C_i\\)" with \\(\text{succ}\ C_i = \lambda x. \lambda s. s\ n\ s\\), however I am not sure if it works.

__Adition__ can be implemented by \\(\text{plus} = \lambda x. \lambda y. \lambda s. \lambda z = x s \left( y\ s\ z \right)\\) which is just "apply \\(s\\) over \\(z\\) as many times as the second parameter indicates, then apply \\(s\\) to it". __Multiplication__ is \\(\text{times} = \lambda x. \lambda y. m \left( \text{plus}\ y \right) C_0 \\).

### Exercise 5.2.3 ###

> If we expand \\(\text{times}\\) to \\(\lambda C_i. \lambda C_j. \lambda s. \lambda z.\\), we can define the helper function \\(C_j\ s\\) which "apply \\(s\\) \\(j\\)-times to whatever is given to it". Finally, we apply that helper function \\(i\\)-times over \\(z\\) resulting in \\(\text{times} = \lambda C_i. \lambda C_j. \lambda s. \lambda z. C_i \left( C_j\ s \right) z\\).

### Exercise 5.2.4 ###

> Following the original \\(\text{times}\\) definition, we define \\(\text{pow} = \lambda C_i. \lambda C_j. C_i \left( \text{times}\  C_j \right) C_1\\).

To build a function that identify \\(C_0\\) we use the fact that \\(C_0\\) ignores its first parameter. Then \\(\text{iszro} = \lambda C_i. C_i \left( \lambda x. fls \right) tru\\). If \\(C_i = C_0\\), the helper function is never apply and \\(\text{iszro} = \text{tru}\\). On the other hand, if \\(C_i \neq C_0\\), the helper function is applied at least once and its last application return \\(fls\\).

The __predecesor__ is significally more complex than any other definition so far.
\\[
\text{zz} = \text{pair}\ C_0 C_0
\\]
\\[
\text{ss} = \lambda p. \text{pair}\ \left( \text{snd}\ p \right)\ \left( \text{plus}\ C_1\ \left( \text{snd}\ p \right) \right)
\\]
\\[
\text{prd}\ = \lambda m. \text{fst}\ \left( m\ \text{ss}\ \text{zz} \right)
\\]

Lets analise \\(m\ \text{ss}\ \text{zz} \\) first. Clearly is just apply \\(ss\\) over \\(zz\\) as many times as \\(m\\) indicates. \\(zz\\) is a pair of zeros. \\(ss\\) takes a pair and return another pair where its first element is a copy of the second element of the parameter and its second element is the _succesor_ of the first element (build by adition). The secuence of iterative applications of \\(ss\\) returns the set of mappings that \\(prd\\) must encode: \\(\\\{ \left( C_0, C_0 \right), \left( C_0, C_1 \right), \left( C_1, C_2 \right), \.\.\. \\\}\\) where the first element must be its output. After generating the necessary tuples, \\(\text{prd}\\) returns the first element of the last generated tuple.

### Exercise 5.2.5 ###

> The \\(\text{sub}\\) can be build with \\(\text{prd}\\) in a fairly straight manner. \\(\text{sub} = \lambda C_i. \lambda C_j. C_j\ \text{prd}\ C_i\\) which apply the \\(\text{prd}\\) function \\(j\\)-times starting with \\(C_i\\). If \\(C_i < C_j\\), then \\(\text{sub}\ C_i\ C_j = C_0\\) which seems reasonable because our lack of negative numbers.

### Exercise 5.2.6 ###

> The number of evaluations grows linearly, \\(\mathcal{O}\left( n \right)\\).

### Exercise 5.2.7 ###

> Using the helper function \\(\text{lowerOrEqual} = \lambda x. \lambda y. \text{iszro} \left( \text{sub}\ x\ y \right)\\) which is \\(\text{tru}\\) if and only of \\(x \leq y\\), we can build \\(\text{equal} = \lambda x. \lambda y. \text{and}\ \left( \text{lowerOrEqual}\ x\ y \right) \left( \text{lowerOfEqual}\ y\ x \right)\\). \\(\text{equal}\\) checks if \\(x \leq y \wedge y \leq x\\) which can only be true if \\(x = y\\).

### Exercise 5.2.8 ###

> \\[
> \text{nil} = \lambda c. \lambda n. n
> \\]
> An useful property is \\(\text{nil}\ \\_ \ \text{nil} = \text{nil}\\)
> \\[
> \text{cons} = \lambda e. \lambda L. \lambda c. \lambda n. c\ e \left( L\ c\ n \right)
> \\]
> \\[
> \text{head} = \lambda L. L\ \text{fst}\ \text{nil}
> \\]
> \\[
> \text{isnil} = \lambda L. L \left( \lambda a. \lambda b. \text{fls} \right) \text{tru}
> \\]
> \\(\text{tail} = \text{All elements of the list except the first one} \\) is more complex, however with the book hint is not so hard to figure it out. The idea is to build pairs of the form \\(\left( \text{full list}, \text{tail of full list} \right)\\).
> \\[
> \text{nn} = \text{pair}\ \text{nil}\ \text{nil}\ 
> \\]
> \\[
> \text{cc} = \lambda e. \lambda p. \text{pair}\ \left( \text{cons}\ e \left( \text{fst}\ p \right) \right) \left( \text{fst}\ p \right)
> \\]
> \\[
> \text{tail} = \lambda L. \text{snd} \left( L\ \text{cc}\ \text{nn} \right)
> \\]

